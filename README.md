# SSL discovery

## Usage

It basically asks services if their are running and gets virtual hosts which they are listening as. E.g.:

```json
lukapo@gitlab:/home/lukapo$ /tmp/discover_ssl
{"data":[{"{#SERVERNAME}":"gitlab.lukapo.cz","{#SERVERPORT}":25,"{#SERVERPROTO}":"smtp"},{"{#SERVERNAME}":"gitlab.lukapo.cz","{#SERVERPORT}":443,"{#SERVERPROTO}":"https"}]}
```

### Features

It can parse following services:

- `/usr/bin/gitlab-ctl`
- `/usr/sbin/nginx`
- `/usr/sbin/apache2ctl`
- `proxmox` - checks if `pveproxy` port is opened (8006)
- `postfix` - checks if obvious ports are opened (25, 587, 465)
- `dovecot` - checks if obvious ports are opened (IMAP: 143, 993; POP3: 110, 995)
- `mongodb` - checks if obvious ports are opened (27017, 27018, 27019)

You can add a command line arguments:
```yaml
-C, --config <path>:     Path to a configuration YAML file.
-h, --help:              Print this help message.
-V, --version:           Print version of discover_ssl.
```

Example of configuration file:
```yaml
ssl_discovery_ignore:
  - SERVERNAME: test.example.net
    SERVERPROTO: http

  - SERVERNAME: test.example.net
    SERVERPROTO: smtp

  - SERVERNAME: test.example.net
    SERVERPROTO: imap
```

## Installation

```bash
wget https://gitlab.lukapo.cz/beranm/ssl_discovery/raw/master/discover_ssl?inline=false -O /opt/discover_ssl
chown root.root /opt/discover_ssl
chmod u+s /opt/discover_ssl
```

Latest binary is also available at: https://s3-eu-west-1.amazonaws.com/deploy-server/bin/discover_ssl_latest

For the version, check: https://s3-eu-west-1.amazonaws.com/deploy-server/bin/latest_version.txt


## Zabbix integration

```bash
mkdir -p /etc/zabbix/zabbix_agentd.d/
echo 'UserParameter=discover.vhosts,/opt/discover_ssl 2>/dev/null' > /etc/zabbix/zabbix_agentd.d/userparameter_ssl_check.conf
echo 'UserParameter=discover.dovecot,/opt/discover_ssl 2>/dev/null' >> /etc/zabbix/zabbix_agentd.d/userparameter_ssl_check.conf
echo 'UserParameter=discover.postfix,/opt/discover_ssl 2>/dev/null' >> /etc/zabbix/zabbix_agentd.d/userparameter_ssl_check.conf
echo 'UserParameter=discover.mongodb,/opt/discover_ssl 2>/dev/null' >> /etc/zabbix/zabbix_agentd.d/userparameter_ssl_check.conf
echo 'UserParameter=discover.proxmox,/opt/discover_ssl 2>/dev/null' >> /etc/zabbix/zabbix_agentd.d/userparameter_ssl_check.conf
```
